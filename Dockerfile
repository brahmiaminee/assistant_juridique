# Utiliser l'image Node.js Alpine
FROM node:20-alpine
# Définir le répertoire de travail
WORKDIR /app
# Installer les outils de build et les dépendances nécessaires
RUN apk add --no-cache python3 make g++
# Copier le fichier package.json et package-lock.json (si disponible)
COPY package*.json ./
# Installer les dépendances
RUN npm i
# Copier les autres fichiers du projet
COPY . .
# Compiler le TypeScript, si nécessaire
RUN npm run build
# Exposer le port que votre app utilise
EXPOSE 3000
# Commande pour démarrer votre application
CMD ["npm", "start"]
