import { checkElasticsearchHealth } from './config/elasticsearch.config';
import app from './app';
import dotenv from 'dotenv';
dotenv.config();

const PORT = process.env.APP_PORT
// Start the server
app.listen(PORT, async () => {
    console.log(`Listening on port ${PORT} 🆗`);
    await checkElasticsearchHealth()
});
