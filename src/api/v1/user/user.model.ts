import mongoose from 'mongoose';
import { IUser } from './user.interfaces'

const userSchema = new mongoose.Schema(
    {
        email: { type: String, required: true, unique: true },
        password: { type: String, required: true },
        firstName: { type: String },
        lastName: { type: String },
        phone: { type: String },
        photo: { type: String },
    },
    {
        timestamps: true, // enable automatic timestamp fields
    }
);

export const UserModel = mongoose.model<IUser>('User', userSchema);
