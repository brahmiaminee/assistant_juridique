import { Router } from 'express'
import { UserController } from './user.controller'
import { verifyJwt } from '../../../middleware/jwt.middleware';
import { upload } from '../../../middleware/multer.middleware';

const router: Router = Router();

router.get('/', verifyJwt, UserController.getAllUsers);
router.get('/:id', verifyJwt, UserController.getUserById);
router.delete('/:id', verifyJwt, UserController.deleteUserById);
router.put('/:id', upload.single('photo'), verifyJwt, UserController.updateUserById);

export default router;