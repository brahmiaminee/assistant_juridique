import { IUser } from 'api/v1/user/user.interfaces'
import { UserModel } from './user.model'


export class UserService {

    public static async getAllUsers(): Promise<IUser[]> {
        return await UserModel.find()
    }

    public static async getUserById(id: string): Promise<IUser> {
        return await UserModel.findById(id)
    }

    public static async deleteUserById(id: string): Promise<IUser> {
        return await UserModel.findByIdAndDelete(id)
    }

    public static async updateUserById(id: string, userData: any): Promise<IUser> {
        return UserModel.findByIdAndUpdate(id, userData, { new: true }).exec();
    }
}
