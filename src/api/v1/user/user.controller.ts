import { Request, Response } from 'express';
import { UserService } from './user.service'
import { sanitizeUser } from '../../../utils/sanitizeUser';
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'

export class UserController {

    public static async getAllUsers(req: Request, res: Response): Promise<Response> {
        try {
            const users = await UserService.getAllUsers();

            // Sanitize each user in the array
            const sanitizedUsers = users.map(user => sanitizeUser(user));

            return res.status(200).json(sanitizedUsers);
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }

    public static async getUserById(req: Request, res: Response): Promise<Response> {
        try {
            const user = await UserService.getUserById(req.params.id);

            // Sanitize each user in the array
            const sanitizedUser = sanitizeUser(user);

            return res.status(200).json(sanitizedUser);
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }

    public static async deleteUserById(req: Request, res: Response): Promise<Response> {
        try {
            const user = await UserService.deleteUserById(req.params.id);
            if (user) {
                return res.status(200).json({ message: "user is deleted" });
            }
            return res.status(200).json({ message: "user not found" });
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }

    public static async updateUserById(req: Request, res: Response): Promise<Response> {
        try {
            let photo;
            if (req.file) {
                photo = '/uploads/' + req.file.filename;
            }

            // Ensure the request has an ID parameter
            const userId = req.params.id;
            if (!userId) {
                return res.status(400).json({ error: "User ID is required in the request parameter" });
            }

            // Check if data provided is valid
            const updateData = { ...req.body, photo };
            if (!updateData) {
                return res.status(400).json({ error: "Update data is required in the request body" });
            }
            let updatedUser = null
            if (updateData.password) {
                const { password } = updateData;
                const hashedPassword = await bcrypt.hash(password, 10)
                const newUser = { ...updateData, password: hashedPassword }
                updatedUser = await UserService.updateUserById(userId, newUser);

            } else {
                updatedUser = await UserService.updateUserById(userId, updateData);
            }
            if (!updatedUser) {
                return res.status(404).json({ error: "User not found or update failed" });
            }
            // Sanitize the user before sending it back
            const user = sanitizeUser(updatedUser);
            const token = jwt.sign(
                { user },
                process.env.SECRET_KEY,
                { expiresIn: '3d' }
            );
            return res.status(201).json({ token, user });
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }

}
