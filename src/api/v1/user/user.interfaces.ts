import { Document, Types } from 'mongoose';

export interface IUser extends Document {
    _id: Types.ObjectId;
    email: string;
    password: string;
    firstName?: string;
    lastName?: string;
    phone?: string;
    createdAt: Date;
    updatedAt: Date;
}