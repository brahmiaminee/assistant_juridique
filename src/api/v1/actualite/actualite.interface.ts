import { Document, Types } from 'mongoose';

export interface IActualite extends Document {
    _id: Types.ObjectId;
    title: string;
    description: string;
    langue: string;
    url?: string;
    file?: string;
    createdAt: Date;
    updatedAt: Date;
}