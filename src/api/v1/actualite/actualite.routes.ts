import { Router } from 'express'
import { ActualiteController } from './actualite.controller'
import { verifyJwt } from '../../../middleware/jwt.middleware';
import { upload } from '../../../middleware/multer.middleware';

const router: Router = Router();

router.post('/', verifyJwt, upload.single('image'), ActualiteController.saveActualite);
router.get('/', verifyJwt, ActualiteController.getAllActualite);
router.get('/:id', verifyJwt, ActualiteController.getActualiteById);
router.delete('/:id', verifyJwt, ActualiteController.deleteActualiteById);
router.put('/:id', verifyJwt, upload.single('image'), ActualiteController.updateActualiteById);


export default router;