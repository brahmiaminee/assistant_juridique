import { Request, Response } from 'express';
import { ActualiteService } from './contact.service'

export class ActualiteController {

    public static async getAllActualite(req: Request, res: Response): Promise<Response> {
        try {
            const actualites = await ActualiteService.getAllActualite();
            return res.status(200).json(actualites);
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }

    public static async saveActualite(req: Request, res: Response): Promise<Response> {
        try {
            // File handling
            let image;
            if (req.file) {
                image = '/uploads/' + req.file.filename;
            }
            const actualite = await ActualiteService.saveActualite({
                ...req.body,
                image,  // this assumes your register method can handle this property
            });

            return res.status(201).json(actualite);
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }

    public static async getActualiteById(req: Request, res: Response): Promise<Response> {
        try {
            const actualites = await ActualiteService.getActualiteById(req.params.id);
            return res.status(200).json(actualites);
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }

    public static async deleteActualiteById(req: Request, res: Response): Promise<Response> {
        try {
            const actualite = await ActualiteService.deleteActualiteById(req.params.id);
            if (actualite) {
                return res.status(200).json({ message: "actualite is deleted" });
            }
            return res.status(200).json({ message: "actualite not found" });
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }

    public static async updateActualiteById(req: Request, res: Response): Promise<Response> {
        try {
            let image;
            if (req.file) {
                image = '/uploads/' + req.file.filename;
            }

            // Ensure the request has an ID parameter
            const actualiteId = req.params.id;
            if (!actualiteId) {
                return res.status(400).json({ error: "actualite ID is required in the request parameter" });
            }

            // Check if data provided is valid
            const updateData = { ...req.body, image };
            if (!updateData) {
                return res.status(400).json({ error: "Update data is required in the request body" });
            }

            // Attempt to update the user
            const updatedActualite = await ActualiteService.updateActualiteById(actualiteId, updateData);

            // Check if update was successful
            if (!updatedActualite) {
                return res.status(404).json({ error: "User not found or update failed" });
            }

            return res.status(200).json(updatedActualite);
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }

}
