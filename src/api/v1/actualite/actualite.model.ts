import mongoose from 'mongoose';
import { IActualite } from './actualite.interface';

const actualiteSchema = new mongoose.Schema(
    {
        title: { type: String, required: true },
        description: { type: String, required: true },
        langue: { type: String, required: true },
        url: { type: String },
        image: { type: String },
    },
    {
        timestamps: true, // enable automatic timestamp fields
    }
);

// Exporting the model based on the schema
export const ActualiteModel = mongoose.model<IActualite>('News', actualiteSchema);
