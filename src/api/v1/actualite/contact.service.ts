import { IActualite } from './actualite.interface'
import { ActualiteModel } from './actualite.model'


export class ActualiteService {

    public static async saveActualite(actualite: IActualite): Promise<IActualite> {
        return await ActualiteModel.create(actualite)
    }

    public static async getAllActualite(): Promise<IActualite[]> {
        return await ActualiteModel.find()
    }

    public static async getActualiteById(id: string): Promise<IActualite> {
        return await ActualiteModel.findById(id)
    }

    public static async deleteActualiteById(id: string): Promise<IActualite> {
        return await ActualiteModel.findByIdAndDelete(id)
    }

    public static async updateActualiteById(id: string, userData: any): Promise<IActualite> {
        return ActualiteModel.findByIdAndUpdate(id, userData, { new: true }).exec();
    }
}
