import openai from '../../../config/openai.config'
import { ElasticService } from '../elastic/elastic.service';
import { HistoryService } from '../history/history.service';
import { Information } from './information.interface'
import { promptFormatter } from '../../../utils/promptFormatter';
import { v4 as uuidv4 } from 'uuid';
const myUuid = uuidv4();

export class OpenAIService {

    public static async generateText(prompt: string, langue: string, information: Information, userId: string) {
        const text = promptFormatter(prompt, langue, information);
        try {
            // Input Validation
            const chatCompletion = await openai.chat.completions.create({
                messages: [{ role: 'user', content: text }],
                model: 'gpt-3.5-turbo',
            });
            const objHistory = {
                "userId": userId,
                "title": myUuid,
                "question": prompt,
                "response": chatCompletion.choices[0].message.content
            }
            // save history
            await HistoryService.saveHistory(objHistory, userId)
            //save to elk
            await ElasticService.saveData(objHistory)
            return chatCompletion.choices[0].message.content;
        } catch (error) {
            console.error('Error calling OpenAI API:', error);
            throw error;
        }
    }
}
