import { Request, Response } from 'express';
import { OpenAIService } from './OpenAI.service';
import { HistoryService } from '../history/history.service';
import { AuthRequest } from 'middleware/jwt.middleware';
import { ElasticService } from '../elastic/elastic.service';
import { buildQuery } from '../../../utils/buildQuery'
import { checkElasticsearchHealth } from '../../../config/elasticsearch.config';

export class OpenAIController {
    public static async generateResponse(req: AuthRequest, res: Response): Promise<Response> {
        try {
            const userId = req.user.user._id
            const prompt: string = req.body.prompt;
            const langue: string = req.body.langue;
            const information = req.body.information

            const query = buildQuery(prompt)
            //Check health

            const isElasticsearchHealthy = await checkElasticsearchHealth();

            if (isElasticsearchHealthy) {
                try {
                    const elasticHistory = await ElasticService.getAllData(query)
                    const isElasticHistory = elasticHistory.hits.hits;

                    if (isElasticHistory.length > 0) {
                        console.log("elk exist");
                        return res.status(200).json({ response: elasticHistory });
                    }
                } catch (error) {
                    console.log("ena mouch mawjoud")
                }
            }
            console.log("elk not exist")
            // Handle the case where either Elasticsearch is not healthy or there is no history.
            const generatedText = await OpenAIService.generateText(prompt, langue, information, userId);
            return res.status(200).json({ response: generatedText });


        } catch (error) {
            console.error('Error generating text:', error);
            // Send a more detailed error message in a development environment
            return res.status(500).json(process.env.NODE_ENV === 'development' ?
                { error: 'Internal Server Error', details: error.message } :
                { error: 'Internal Server Error', details: error });
        }
    }
}
