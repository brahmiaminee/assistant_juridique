import { Router } from 'express'
import { OpenAIController } from './OpenAI.controller'
import { verifyJwt } from '../../../middleware/jwt.middleware';



const router: Router = Router();

router.post('/', verifyJwt, OpenAIController.generateResponse);


export default router;