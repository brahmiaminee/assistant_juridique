import mongoose from 'mongoose';
import { IContact } from './contact.interface';

const contactSchema = new mongoose.Schema(
    {
        objet: { type: String, required: true },
        description: { type: String, required: true },
        file: { type: String },
        email: { type: String },
        phone: { type: String },

    },
    {
        timestamps: true, // enable automatic timestamp fields
    }
);

// Exporting the model based on the schema
export const ContactModel = mongoose.model<IContact>('Contact', contactSchema);
