import { Request, Response } from 'express';
import { ContactService } from './contact.service'

export class ContactController {

    public static async getAllContact(req: Request, res: Response): Promise<Response> {
        try {
            const contacts = await ContactService.getAllContact();
            return res.status(200).json(contacts);
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }

    public static async saveContact(req: Request, res: Response): Promise<Response> {
        try {
            // File handling
            let file;
            if (req.file) {
                file = '/uploads/' + req.file.filename;
            }
            const contact = await ContactService.saveContact({
                ...req.body,
                file,  // this assumes your register method can handle this property
            });

            return res.status(201).json(contact);
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }

    public static async getContactById(req: Request, res: Response): Promise<Response> {
        try {
            const contacts = await ContactService.getContactById(req.params.id);
            return res.status(200).json(contacts);
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }

    public static async deleteContactById(req: Request, res: Response): Promise<Response> {
        try {
            const contact = await ContactService.deleteContactById(req.params.id);
            if (contact) {
                return res.status(200).json({ message: "Contact is deleted" });
            }
            return res.status(200).json({ message: "Contact not found" });
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }

    public static async updateContactById(req: Request, res: Response): Promise<Response> {
        try {
            let file;
            if (req.file) {
                file = '/uploads/' + req.file.filename;
            }

            // Ensure the request has an ID parameter
            const contactId = req.params.id;
            if (!contactId) {
                return res.status(400).json({ error: "Contact ID is required in the request parameter" });
            }

            // Check if data provided is valid
            const updateData = { ...req.body, file };
            if (!updateData) {
                return res.status(400).json({ error: "Update data is required in the request body" });
            }

            // Attempt to update the user
            const updatedContact = await ContactService.updateContactById(contactId, updateData);

            // Check if update was successful
            if (!updatedContact) {
                return res.status(404).json({ error: "User not found or update failed" });
            }

            return res.status(200).json(updatedContact);
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }

}
