import { Router } from 'express'
import { ContactController } from './contact.controller'
import { verifyJwt } from '../../../middleware/jwt.middleware';
import { upload } from '../../../middleware/multer.middleware';

const router: Router = Router();

router.post('/', verifyJwt, upload.single('file'), ContactController.saveContact);
router.get('/', verifyJwt, ContactController.getAllContact);
router.get('/:id', verifyJwt, ContactController.getContactById);
router.delete('/:id', verifyJwt, ContactController.deleteContactById);
router.put('/:id', verifyJwt, upload.single('file'), ContactController.updateContactById);


export default router;