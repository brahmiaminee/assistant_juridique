import { IContact } from './contact.interface'
import { ContactModel } from './contact.model'


export class ContactService {

    public static async saveContact(contact: IContact): Promise<IContact> {
        return await ContactModel.create(contact)
    }

    public static async getAllContact(): Promise<IContact[]> {
        return await ContactModel.find()
    }

    public static async getContactById(id: string): Promise<IContact> {
        return await ContactModel.findById(id)
    }

    public static async deleteContactById(id: string): Promise<IContact> {
        return await ContactModel.findByIdAndDelete(id)
    }

    public static async updateContactById(id: string, userData: any): Promise<IContact> {
        return ContactModel.findByIdAndUpdate(id, userData, { new: true }).exec();
    }
}
