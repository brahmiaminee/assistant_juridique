import { Document, Types } from 'mongoose';

export interface IContact extends Document {
    _id: Types.ObjectId;
    objet: string;
    description: string;
    file?: string;
    email?: string;
    phone?: string;
    createdAt: Date;
    updatedAt: Date;
}