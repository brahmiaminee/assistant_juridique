import { Document, Types } from 'mongoose';

export interface IHistory extends Document {
    _id?: Types.ObjectId;
    userId: string;
    title: string;
    question?: string;
    response?: string;
    createdAt: Date;
    updatedAt: Date;
}

export interface ICreateHistory {
    userId: string;
    title: string;
    question: string;
    response: string;
}
