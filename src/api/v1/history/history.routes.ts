import { Router } from 'express'
import { HistoryController } from './history.controller'
import { verifyJwt } from '../../../middleware/jwt.middleware';

const router: Router = Router();

router.post('/', verifyJwt, HistoryController.saveHistory);
router.get('/', verifyJwt, HistoryController.getAllHistory);
router.get('/:id', verifyJwt, HistoryController.getHistoryById);
router.delete('/:id', verifyJwt, HistoryController.deleteHistoryById);
router.put('/:id', verifyJwt, HistoryController.updateHistoryById);
router.get('/user/:id', verifyJwt, HistoryController.getHistoryByUserId);
router.delete('/user/:id', verifyJwt, HistoryController.deleteHistoryByUserId);

export default router;