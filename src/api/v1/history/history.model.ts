import mongoose from 'mongoose';
import { IHistory } from './history.interface';

const historySchema = new mongoose.Schema(
    {
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true
        },
        title: { type: String, required: true },
        question: { type: String, required: true },
        response: { type: String, required: true },

    },
    {
        timestamps: true, // enable automatic timestamp fields
    }
);

// Exporting the model based on the schema
export const HistoryModel = mongoose.model<IHistory>('History', historySchema);
