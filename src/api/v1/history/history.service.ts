import { ICreateHistory, IHistory } from './history.interface'
import { HistoryModel } from './history.model'


export class HistoryService {

    public static async saveHistory(history: ICreateHistory, userId: string): Promise<IHistory> {
        return await HistoryModel.create({ ...history, userId })
    }

    public static async getAllHistory(): Promise<IHistory[]> {
        return await HistoryModel.find()
    }

    public static async getHistoryById(id: string): Promise<IHistory> {
        return await HistoryModel.findById(id)
    }

    public static async getHistoryByUserId(userId: string): Promise<IHistory[]> {
        return await HistoryModel.find({ userId })
    }

    public static async deleteHistoryById(id: string): Promise<IHistory> {
        return await HistoryModel.findByIdAndDelete(id)
    }
    public static async deleteHistoryByUserId(userId: string): Promise<any> {
        return await HistoryModel.deleteMany({ userId })
    }

    public static async updateHistoryById(id: string, userData: any): Promise<IHistory> {
        return HistoryModel.findByIdAndUpdate(id, userData, { new: true }).exec();
    }
}
