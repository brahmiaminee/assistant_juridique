import { Request, Response } from 'express';
import { HistoryService } from './history.service'
import redisClient from '../../../config/redis.config';
import { AuthRequest } from 'middleware/jwt.middleware';

export class HistoryController {

    public static async getAllHistory(req: Request, res: Response): Promise<Response> {
        try {
            const histories = await HistoryService.getAllHistory();
            return res.status(200).json(histories);
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }

    public static async saveHistory(req: AuthRequest, res: Response): Promise<Response> {
        try {
            const history = await HistoryService.saveHistory(req.body, req.user.user._id);
            return res.status(201).json(history);
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }

    public static async getHistoryById(req: Request, res: Response): Promise<Response> {
        try {
            const history = await HistoryService.getHistoryById(req.params.id);
            return res.status(200).json(history);
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }

    public static async deleteHistoryById(req: Request, res: Response): Promise<Response> {
        try {
            const user = await HistoryService.deleteHistoryById(req.params.id);
            if (user) {
                return res.status(200).json({ message: "history is deleted" });
            }
            return res.status(200).json({ message: "history not found" });
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }

    public static async updateHistoryById(req: Request, res: Response): Promise<Response> {
        try {
            // Ensure the request has an ID parameter
            const userId = req.params.id;
            if (!userId) {
                return res.status(400).json({ error: "History ID is required in the request parameter" });
            }

            // Check if data provided is valid
            const updateData = req.body
            if (!updateData) {
                return res.status(400).json({ error: "Update data is required in the request body" });
            }

            // Attempt to update the user
            const updatedHistory = await HistoryService.updateHistoryById(userId, updateData);


            // Check if update was successful
            if (!updatedHistory) {
                return res.status(404).json({ error: "History not found or update failed" });
            }


            return res.status(200).json(updatedHistory);
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }

    public static async getHistoryByUserId(req: Request, res: Response): Promise<Response> {
        try {
            const userId = req.params.id;
            const reply = await redisClient.get(userId);
            if (reply) {
                console.log("Using cached data");
                return res.status(200).json(JSON.parse(reply));
            }
            console.log("no cache")
            const history = await HistoryService.getHistoryByUserId(req.params.id);
            await redisClient.setex(userId, 10, JSON.stringify(history));
            return res.status(200).json(history);

        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }

    public static async deleteHistoryByUserId(req: Request, res: Response): Promise<Response> {
        try {
            const user = await HistoryService.deleteHistoryByUserId(req.params.id);
            if (user) {
                return res.status(200).json({ message: "histories is deleted" });
            }
            return res.status(200).json({ message: "histories not found" });
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }
}
