import { Request, Response } from 'express';
import { ElasticService } from './elastic.service'

export class ElasticController {

    public static async getAllData(req: Request, res: Response): Promise<Response> {
        try {
            const histories = await ElasticService.getAllData(req.body);
            return res.status(200).json(histories.hits.hits);
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }


    public static async saveData(req: Request, res: Response): Promise<Response> {
        try {
            const history = await ElasticService.saveData(req.body);
            return res.status(201).json(history);
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }


}
