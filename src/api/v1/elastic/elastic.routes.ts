import { Router } from 'express'
import { ElasticController } from './elastic.controller'
import { verifyJwt } from '../../../middleware/jwt.middleware';

const router: Router = Router();

router.post('/', verifyJwt, ElasticController.saveData);
router.get('/', verifyJwt, ElasticController.getAllData);

export default router;