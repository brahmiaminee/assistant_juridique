import { ElasticsearchResponse } from 'api/v1/elastic/elastic.interface';
import { checkElasticsearchHealth, elasticClient } from '../../../config/elasticsearch.config';
import { ICreateHistory, IHistory } from '../history/history.interface'
import { HistoryModel } from '../history/history.model'


export class ElasticService {

    public static async saveData(history: any): Promise<void> {
        try {
            await checkElasticsearchHealth();
            return await elasticClient.index({
                index: 'history',
                body: history
            });
        } catch (error) {
            console.error('Error indexing data:', error);
        }
    }

    public static async getAllData(queryObject: object): Promise<ElasticsearchResponse<IHistory>> {
        try {
            return await elasticClient.search({
                index: 'history',
                body: queryObject
            });

        } catch (error) {
            console.error('Error fetching data:', error);
            throw error;  // Re-throw the error so it can be handled upstream
        }
    }
}
