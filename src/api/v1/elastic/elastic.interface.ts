export interface ElasticsearchResponse<T> {
    hits: {
        hits: T[];
    };
}
