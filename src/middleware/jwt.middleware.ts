import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
dotenv.config();

export interface AuthRequest extends Request {
    user?: any; // ou un type plus spécifique si vous avez une structure utilisateur définie
}

export const verifyJwt = (req: AuthRequest, res: Response, next: NextFunction) => {
    const token = req.header('Authorization')?.split('Bearer ')[1];

    if (!token) {
        return res.status(401).json({ message: 'Access denied. No token provided.' });
    }

    try {
        const decoded = jwt.verify(token, process.env.SECRET_KEY,); // Replace with your secret key
        req.user = decoded; // Optionally add the decoded payload to the request
        next(); // Pass control to the next middleware function
    } catch (error) {
        res.status(400).json({ message: 'Invalid token.' });
    }
};
