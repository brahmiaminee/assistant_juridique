import mongoose from 'mongoose'
import dotenv from 'dotenv';
dotenv.config();

const connectDB = async () => {
    const connectionString = process.env.MONGO_URL
    try {
        await mongoose.connect(connectionString);
        console.log('Connected to MongoDB: 🆗');
    } catch (error) {
        console.error('Could not connect to MongoDB 🚫', error);
        process.exit(1); // Exit the process with failure
    }
};

export default connectDB;
