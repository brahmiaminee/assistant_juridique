const { Client } = require('@elastic/elasticsearch')
import dotenv from 'dotenv';
dotenv.config();

export const elasticClient = new Client({
    node: process.env.ELASTICSEARCH_URL,
    auth: {
        username: process.env.ELASTICSEARCH_USERNAME,
        password: process.env.ELASTICSEARCH_PASSWORD
    }
})


export const checkElasticsearchHealth = async (): Promise<boolean> => {
    try {
        const health = await elasticClient.cluster.health({});
        console.log('Elasticsearch client connected: 🆗');
        // Here you might want to check for specific properties of the health object
        // for example: health.status should be 'green' or 'yellow'
        return true;
    } catch (error) {
        console.error('Cannot connect to Elasticsearch: 🚫');
        return false;
    }
};
