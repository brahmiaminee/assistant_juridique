import { Redis } from 'ioredis';
import dotenv from 'dotenv';
dotenv.config();

const redisClient = new Redis({
    host: process.env.REDIS_USER,
    port: Number(process.env.REDIS_PORT)
});

redisClient.on('error', (err) => {
    console.error('Redis error:', err.message);
});

redisClient.on('connect', () => {
    console.log('Redis client connected: 🆗');
});


redisClient.on('close', () => {
    console.log('Redis client disconnected 🚫');
});

export default redisClient;
