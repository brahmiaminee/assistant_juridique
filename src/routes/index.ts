import { Router, Request, Response } from 'express';
import userRoutes from '../api/v1/user/user.routes';
import authRoutes from '../Auth/auth.routes';
import openAIRoutes from '../api/v1/openai/openAI.routes';
import historyRoutes from '../api/v1/history/history.routes';
import elasticRoutes from '../api/v1/elastic/elastic.routes';
import contactRoutes from '../api/v1/contact/contact.routes';
import actualiteRoutes from '../api/v1/actualite/actualite.routes';
import authRouter from '../Auth/googleAuth/googleAuth.routes';

const router: Router = Router();

router.get("/first", (req: Request, res: Response) => {
    res.send("first");
});

router.use('/user', userRoutes);
router.use('/', authRoutes);
router.use('/ai', openAIRoutes);
router.use('/history', historyRoutes);
router.use('/elastic', elasticRoutes);
router.use('/contact', contactRoutes);
router.use('/news', actualiteRoutes);
router.use('/auth', authRouter);

export default router;
