import express, { Request, Response } from "express";
import routes from './routes/index';
import connectDB from "./config/db";
import bodyParser from 'body-parser';
import passport from "passport";
import session from 'express-session';
import dotenv from 'dotenv';
dotenv.config();
import "./Auth/googleAuth/passport";
import cors from 'cors';

const app = express();

app.use(bodyParser.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true }));

app.use(cors({
    origin: 'http://localhost:3001', // or your React app's origin
    methods: '*',
    credentials: true,
}));


// Connect to the database
connectDB();

// Middlewares
app.use(express.json());

//
app.use(session({
    secret: process.env.SECRET_KEY,
    resave: false,
    saveUninitialized: false,
    cookie: { secure: 'auto' }
}));
app.use(passport.initialize());  // Initialize Passport
app.use(passport.session());  // Enable session support for Passport

// Routes
app.get('/', (req: Request, res: Response) => {
    res.send('Hello World!');
});

app.use("/uploads", express.static(__dirname + "/uploads/"));

// Prefixing all routes with /api/v1
app.use('/api/v1', routes);

// Export the configured app
export default app;
