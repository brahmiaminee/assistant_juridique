import { IUser } from '../api/v1/user/user.interfaces'; // adjust path as necessary

export function sanitizeUser(user: IUser): Partial<IUser> {
    const userObj = user.toObject(); // Convert mongoose document to plain JS object
    delete userObj.password; // Omit the password
    return userObj;
}
