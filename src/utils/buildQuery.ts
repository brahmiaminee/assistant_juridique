export const buildQuery = (prompt: string) => {
    return {
        "query": {
            "more_like_this": {
                "fields": [
                    "question"
                ],
                "like": [
                    {
                        "_index": "history",
                        "doc": {
                            "question": prompt
                        }
                    }
                ],
                "min_term_freq": 1,
                "min_doc_freq": 1
            }
        }
    };
};
