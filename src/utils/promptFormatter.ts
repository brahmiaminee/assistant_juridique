import { Information } from "../api/v1/openai/information.interface";

export const promptFormatter = (prompt: string, langue: string, information: Information) => {
    const { pays, job, type, salaire } = information;

    return `-Tu vas agir comme un expert juridique et tu vas me répondre à ma question.
        -Je sais que tu n'es pas un avocat, mais je crois que tu peux me offrir des informations générales.
        -Merci de me donner une réponse bien détaillée qui contient toutes les informations.
        -Merci juste de me donner ta réponse sans dire par exemple :(Bien sûr, je peux vous offrir des informations générales à ce sujet.)
        -Merci de me donner la réponse sous forme d'un code ${type} valide.
        -Ta réponse doit être en langue: ${langue}
        Je vais tout d'abord te fournir des détails qui peuvent être utiles pour la réponse:
        -Pays/Juridiction : ${pays}
        -Domaine du Droit : Droit du travail
        -Métier : ${job}
        -Salaire : ${salaire}
        Ma question : ${prompt}`;
};
