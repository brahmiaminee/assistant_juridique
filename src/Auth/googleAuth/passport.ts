import { UserModel } from "../../api/v1/user/user.model";
import passport from "passport";
import passportGoogle from "passport-google-oauth20";
const GoogleStrategy = passportGoogle.Strategy;
import bcrypt from 'bcrypt'
import dotenv from 'dotenv';
import { sanitizeUser } from "../../utils/sanitizeUser";
dotenv.config();


passport.use(
    new GoogleStrategy(
        {
            clientID: process.env.GOOGLE_CLIENT_ID,
            clientSecret: process.env.GOOGLE_SECRET_CODE,
            callbackURL: `${process.env.APP_BACK}auth/google/callback`,
        },
        async (accessToken, refreshToken, profile, done) => {
            const email = profile.emails?.[0].value
            const user = await UserModel.findOne({ email: email });
            // If user doesn't exist creates a new user. (similar to sign up)
            if (!user) {
                const newUser = await UserModel.create({
                    email: email,
                    password: await bcrypt.hash(process.env.GOOGLE_DEFAULT_PASS, 10),
                });

                if (newUser) {
                    const sanitizedUser = sanitizeUser(newUser);
                    done(null, sanitizedUser);
                }
            } else {
                const sanitizedUser = sanitizeUser(user);
                done(null, sanitizedUser);
            }
        }
    )
);

passport.serializeUser((user, done) => {
    done(null, user);
});

passport.deserializeUser((user, done) => {
    done(null, user);
});