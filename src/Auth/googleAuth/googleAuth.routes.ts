// routes/authRoutes.ts

import express from 'express';
import passport from 'passport';
import * as authController from './googleAuth.controller';

const router = express.Router();

router.get(
    "/google",
    passport.authenticate("google", {
        scope: ["email", "profile"],
    })

);

router.get('/google/callback', passport.authenticate('google'), authController.handleCallback);
export default router;
