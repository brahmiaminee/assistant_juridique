import jwt from 'jsonwebtoken'
import { Request, Response } from 'express';

interface User {
    _id: string;
    email: string
    // any other properties that your user object might have
}

interface AuthenticatedRequest extends Request {
    user: User;
}

export const handleCallback = (req: AuthenticatedRequest, res: Response) => {
    // Redirect after successfully authenticated with Google.
    const user = req.user
    const token = jwt.sign(
        { user },
        process.env.SECRET_KEY,
        { expiresIn: '3d' }
    );
    return res.redirect(`${process.env.APP_FRONT}?token=${token}`);
};