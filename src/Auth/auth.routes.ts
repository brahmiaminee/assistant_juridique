import { Router } from 'express'
import { AuthController } from './auth.controller'
import { upload } from '../middleware/multer.middleware';

const router: Router = Router();


router.post('/register', upload.single('photo'), AuthController.register);
router.post('/login', AuthController.login);

export default router;