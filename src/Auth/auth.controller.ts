import { Request, Response } from 'express';
import { AuthService } from './auth.service'
import jwt from 'jsonwebtoken'
import dotenv from 'dotenv';
import { sanitizeUser } from '../utils/sanitizeUser';
dotenv.config();

export class AuthController {

    public static async register(req: Request, res: Response): Promise<Response> {
        try {
            // File handling
            let photo;
            if (req.file) {
                photo = '/uploads/' + req.file.filename;
            }

            const user = await AuthService.register({
                ...req.body,
                photo,  // this assumes your register method can handle this property
            });
            const sanitizedUser = sanitizeUser(user);
            const token = jwt.sign(
                { user },
                process.env.SECRET_KEY,
                { expiresIn: '3d' }
            );

            return res.status(201).json({ token, user: sanitizedUser });
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }

    public static async login(req: Request, res: Response): Promise<Response> {
        try {
            const user = await AuthService.login(req.body);
            const sanitizedUser = sanitizeUser(user);
            const token = jwt.sign(
                { user },
                process.env.SECRET_KEY,
                { expiresIn: '3d' }
            );
            return res.status(201).json({ token, user: sanitizedUser });
        } catch (error) {
            return res.status(500).json({ error: error.message });
        }
    }
}
