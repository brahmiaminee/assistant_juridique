import { IUser } from 'api/v1/user/user.interfaces'
import { UserModel } from '../api/v1/user/user.model'
import bcrypt from 'bcrypt'

export class AuthService {
    public static async register(user: IUser): Promise<IUser> {
        const { password } = user;
        const hashedPassword = await bcrypt.hash(password, 10)
        const newUser = { ...user, password: hashedPassword }
        return await UserModel.create(newUser)
    }

    public static async login(user: IUser): Promise<IUser> {
        const { email, password } = user;
        const userLogin = await UserModel.findOne({ email });
        if (!userLogin) {
            throw new Error('User not found');
        }
        const isPasswordCorrect = await bcrypt.compare(password, userLogin.password);
        if (!isPasswordCorrect) {
            throw new Error('Invalid password');
        }
        return userLogin;

    }

}

